from django.urls import path
from .views import create_jadwal, list_jadwal, get_jadwal, delete_jadwal, delete_jadwal_all

#url for app
urlpatterns = [
    path('create/', create_jadwal, name='create_jadwal'),
    path('', list_jadwal, name='list_jadwal'),
    path('<int:id>/', get_jadwal, name='get_jadwal'),
    path('<int:id>/delete/', delete_jadwal, name='delete_jadwal'),
    path('delete/all/', delete_jadwal_all, name='delete_jadwal_all'),
]

