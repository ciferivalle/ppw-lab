from django.shortcuts import render, redirect
from .models import JadwalPribadi
from .forms import JadwalPribadiForm

# Create your views here.
def create_jadwal(request):
    form = JadwalPribadiForm(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            hari = request.POST.get("hari")
            tanggal = request.POST.get("tanggal")
            jam = request.POST.get("jam")
            kategori = request.POST.get("kategori")
            nama_kegiatan = request.POST.get("nama_kegiatan")
            tempat = request.POST.get("tempat")
            key_pass = request.POST.get("key_pass")
            JadwalPribadi.objects.create(hari=hari, tanggal=tanggal, jam=jam, kategori=kategori, nama_kegiatan=nama_kegiatan, tempat=tempat)
            response = {
                "message" : "Jadwal Created",
                "color" : "aqua",
            }
            return redirect('list_jadwal')
        else:
            response = {
                "form" : JadwalPribadiForm,
                "message" : "Form is not Valid",
                "color" : "red",
            }
            return render(request, 'create_jadwal.html', response)

    response['form'] = form
    return render(request, 'create_jadwal.html', response)


def list_jadwal(request):
    jadwals = JadwalPribadi.objects.all()
    response = {
        "jadwals" : jadwals
    }
    return render(request, 'list_jadwal.html', response)

def get_jadwal(request, id):
    response = {}
    try:
        jadwal = JadwalPribadi.objects.get(pk=id)
        response = {
            "jadwal" : jadwal
        }
        return render(request, 'jadwal.html', response)
    except:
        response['message'] = "Jadwal with a given id is not found"
        response["color"] = "red"
        return render(request, 'jadwal.html', response)

def delete_jadwal(request, id):
    jadwal = JadwalPribadi.objects.filter(pk=id).delete()
    response = {
        "jadwal" : jadwal
    }
    return redirect('list_jadwal')
    
def delete_jadwal_all(request):
    jadwal = JadwalPribadi.objects.all().delete()
    return redirect('list_jadwal')


