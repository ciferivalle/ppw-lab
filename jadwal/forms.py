from django import forms
from .models import JadwalPribadi


class JadwalPribadiForm(forms.Form):
    hari = forms.CharField(label="Hari")
    tanggal = forms.DateField(
        label="Tanggal", widget=forms.DateInput(attrs={'type': "date"}))
    jam = forms.TimeField(
        label="Jam", widget=forms.TimeInput(attrs={'type': 'time'}))
    nama_kegiatan = forms.CharField(label="Nama Kegiatan")
    tempat = forms.CharField(label="Tempat")
    kategori = forms.CharField(label="Kategori")
