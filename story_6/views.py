from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.

def landing(request):
    form = StatusForm(request.POST or None)
    if(request.method == "POST"):
        if(form.is_valid()):
            Status.objects.create(status=request.POST.get("status"), name=request.POST.get("name"))
            return redirect('story-6:index')
        else:
            return redirect('story-6:index')

    status = Status.objects.all().order_by("-id")
    response = {
        "status" : status,
        "form" : form
    }
    return render(request, 'landing.html', response)

def profile(request):
    response = {
        "name": "Muhammad Fachry Nataprawira",
        "npm": "1706039704"
    }
    return render(request, 'profile.html', response)
    