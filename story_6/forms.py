from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=2048) 
    status = forms.CharField(max_length=2048) 