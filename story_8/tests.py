from django.test import TestCase
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.urls import resolve
from .views import index
from django.test import Client

client = Client()

# Create your tests here.
class StatusViewTest(TestCase):
    """
    Test views' status code response
    """
    def test_status_view_get_return_200(self):
        """
        Test request get return 200
        """
        response = client.get('/story-8/')
        self.assertEqual(response.status_code, 200)

    def test_status_views_using_landing_func(self):
        """
        Test if a reverse using correct views function
        """
        found = resolve('/story-8/')
        self.assertEqual(found.func, index)

    def test_get_correct_output_html(self):
        response = client.get('/story-8/')
        html_response = response.content.decode('utf8')
        self.assertIn("Biodata", html_response)

class StatusFunctionalTest(LiveServerTestCase):

    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=op)
        # self.selenium.implicitly_wait(10)
        super(StatusFunctionalTest, self).setUp()

    def test_if_theme_is_changed(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-8/'))
        page = selenium.page_source
        self.assertIn('dark.css', page)
        #selenium.find_element_by_xpath('//*[@id="theme-toggler"]').click()
        #page = selenium.page_source
        #self.assertIn('light.css', page)

    def test_accordion_working(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-8/'))
        page = selenium.page_source
        # self.assertIn('ui-state-default', page)
        # selenium.find_element_by_id('accordion-3').click()
        # self.assertIn('ui-state-active', page)

    def test_if_loader_is_loaded(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-8/'))
        page = selenium.page_source
        # self.assertTrue(selenium.find_element_by_id('preloader').is_displayed())
        # self.assertTrue(selenium.find_element_by_tag_name('body').is_displayed())

    def tearDown(self):
        self.selenium.close()
        super(StatusFunctionalTest, self).tearDown()
