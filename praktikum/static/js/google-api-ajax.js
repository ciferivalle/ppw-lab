var counter = 0;
function fav(caller) {
  console.log("clicked")

  // unlike
  if( $(caller).attr('src') == yellow_star) {
      $(caller).attr('src', blue_star);
      unlike($(caller).attr("book_id"))
      $("#favorited").html(counter);
  //like
  } else {
      $(caller).attr('src', yellow_star);
      counter++;
      like($(caller).attr("book_id"))
      $("#favorited").html(counter);
  }
}

// var add_fav_url = "{% url 'story_9:add-to-favorite' %}";
// var rem_fav_url = "{% url 'story_9:remove-from-favorite' %}";
function like(caller, count) {

  console.log($(caller).attr("book_id"))
  data = {
    "liked" : $(caller).attr("book_id"),
  }
  $.ajax({
    type:"POST",
    url:add_fav_url,
    data: data
  })
  .done(function(data) {
    console.log(data);
    load()
  })
}

function unlike(caller, count) {
  console.log($(caller).attr("book_id"))
  data = {
    "liked" : $(caller).attr("book_id"),
  }
  $.ajax({
    type:"POST",
    url:rem_fav_url,
    data: data
  })
  .done(function(data) {
    console.log(data);
    load()
  })
  ;
}
function load() {
  var urlParams = new URLSearchParams(window.location.search);
  $.get({
    url: get_book_json_url + "?q=" + urlParams.get('q'),
    context: document.body,
    success: renderIt,
  });
}

function renderIt(data) {
  $("#favorited").html(data['liked_books'].length);
  $("#books-list").html('')
  console.log(data['liked_books'])
  $.each(data['items'], function(i, item) {
    var count = data['liked_books'].length;
    var toLike = `<img book_id="${item['id']}" class="fav" width="30" src="${blue_star}" alt="" onclick="like(this, ${count})">`
    var toUnlike = `<img book_id="${item['id']}" class="fav" width="30" src="${yellow_star}" alt="" onclick="unlike(this, ${count})">`
    var btnsrc = ($.inArray(item['id'], data['liked_books']) > -1) ? toUnlike:toLike;

    try{
      var img = (
        item["volumeInfo"]["imageLinks"]["smallThumbnail"]) ?
      `
        <td class='text bg-grey'>
          <img width='50px' src='${ item["volumeInfo"]["imageLinks"]["smallThumbnail"] }' alt=''>
        </td>
      `
      : "";
    } catch(err) {
        var img = ""
    }
    $("#books-list").append(
      `
      <tr>
        ${img}
        <td class="text bg-grey">${ item["volumeInfo"]["title"] }</td>
        <td class="text bg-grey">${ item["volumeInfo"]["authors"] }</td>
        <td class="text bg-grey">${ item["volumeInfo"]["publishedDate"] }</td>
        <td class="text bg-grey">
          ${btnsrc}
        </td>
      </tr>
      `)
  });
}

$(document).ready(function() {
  load()
  $('#searchbar').on("keyup", function(e){
    q = e.currentTarget.value
    console.log(q)
    $.get({
      url: get_book_json_url + "?q=" + q,
      context: document.body,
      success: renderIt,
    });
  });
});
