from django.contrib import admin
from django.urls import path, include
from .views import index, check_email, subscribe, list_subscriber, unsubscribe

urlpatterns = [
    path('', index, name='index'),
    path('check-email/', check_email, name='check-email'),
    path('subscribe/', subscribe, name='subscribe'),
    path('subscribers/', list_subscriber, name='list-subscriber'),
    path('subscribers/<int:id>/unsubscribe/', unsubscribe, name='unsubscribe'),
]
