from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError

from .models import Subscriber

# Create your views here.

response = {}
def index(request):
    return render(request, 'index10.html', {})

def check_email(request):
    print("A")
    try:
        validate_email(request.POST['inputValue'])
        print(request.POST['inputValue'])
        print("B")
    except:
        response['message'] = "Format email tidak benar"
        response['status_code'] = 400
        return JsonResponse(response)

    subscriber = Subscriber.objects.filter(email=request.POST['inputValue'])

    if(subscriber):
        print("D")
        response['message'] = "Email tersebut sudah pernah didaftarkan\
         sebelumnya, silakan daftar dengan email lain"
        response['status_code'] = 400
        return JsonResponse(response)

    print("E")
    response['message'] = "Email belum terdaftar, dapat digunakan"
    response['status_code'] = 200
    return JsonResponse(response, status=200)

def subscribe(request):
    if(request.method == "POST"):
        try:
            validate_email(request.POST['email'])
            subscriber = Subscriber.objects.create(name=request.POST['name'],
                                    email=request.POST['email'],
                                    password=request.POST['password'])
            subscriber = serializers.serialize("json", [subscriber, ])
            print(subscriber)

        except (ValidationError, IntegrityError)  as e:
            response['message'] = "Email format is wrong, or email is already exist"
            return JsonResponse(response, status=200)

        response['message'] = "Subscriber created"
        response['subscriber'] = subscriber
        return JsonResponse(response, status=200)

    response['message'] = "GET method not allowed"
    return JsonResponse(response, status=403)

def list_subscriber(request):
    subscribers = Subscriber.objects.all().order_by("-pk")
    subscribers = serializers.serialize('json', subscribers, fields=('pk', 'id', 'name', 'email'))
    return HttpResponse(subscribers)

def unsubscribe(request, id):
    if(request.method == "POST"):
        # get a subscriber with the given id
        try:
            subscriber = Subscriber.objects.get(pk=id)
        except Exception as e:
            print("A")
            print(e)
            # jika gaada subscriber dengan email yang disebutkan
            return JsonResponse({"message": "Tidak ada subscriber dengan email itu"})

        password = request.POST['password']
        print(subscriber)
        if(subscriber.password != password):
            return JsonResponse({"message": "Password Salah"})

        subscriber.delete()
        return JsonResponse({"message": "Email anda selesai di unsubscribe"})


    return JsonResponse({"message": "GET Method is not allowed"})
