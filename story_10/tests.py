import requests
import time

from django.urls import resolve

from django.test import TestCase
from django.test import LiveServerTestCase
from django.test import Client
from django.shortcuts import reverse
from django.db import IntegrityError

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import Subscriber


client = Client()

# Create your tests here.
class StatusViewTest(TestCase):
    """
    Test views' status code response
    """
    def test_index_view_get_return_200(self):
        """
        Test request get return 200
        """
        response = client.get('/story-10/')
        self.assertEqual(response.status_code, 200)

    def test_check_email_view_get_return_200(self):
        """
        Test request get return 200
        """
        response = client.post('/story-10/check-email/', data={
                                    "inputValue": "emailku@gmail.com"
                                })
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        """
        Test request get return 200
        """
        Subscriber.objects.create(name="name",
                                email="emailku@gmail.com",
                                password="password")
        response = client.post('/story-10/check-email/', data={
                                    "inputValue": "emailku@gmail.com"
                                })
        self.assertEqual(response.json()['status_code'], 400)

    def test_check_email_not_valid_400(self):
        """
        Test request get return 200
        """
        response = client.post("/story-10/subscribe/", data={
                                    "name":"name",
                                    "email":"emailku",
                                    "password":"password"})
        self.assertEqual(response.json()['status_code'], 400)

    def test_check_email_not_valid_format_view_get_return_400(self):
        """
        Test request get return 200
        """
        response = client.post('/story-10/check-email/', data={
                                        "inputValue": "emailku1"
                                    })
        self.assertEqual(response.json()['status_code'], 400)

    def test_subscribe_view_get_return_403(self):
        """
        Test request get return 200
        """
        response = client.get('/story-10/subscribe/')
        print(response)
        self.assertEqual(response.status_code, 403)

    def test_subscribe_view_post_return_200(self):
        """
        Test request get return 200
        """
        response = client.post('/story-10/subscribe/', data={
                                                "name" : "name",
                                                "email" : "hello@gmail.com",
                                                "password" : "123456",
                                            })
        self.assertEqual(response.status_code, 200)

    def test_subscribe_views_using_correct_function(self):
        """
        Test if a reverse using correct views function
        """
        found = resolve('/story-10/')
        self.assertEqual(found.func, index)

    def test_get_correct_output_html(self):
        response = client.get('/story-10/')
        html_response = response.content.decode('utf8')
        self.assertIn("Subscribe", html_response)

class ModelTest(TestCase):
    def test_subscribed_user_created(self):
        counter_before_inc = Subscriber.objects.count()
        liked_book = Subscriber.objects.create(email="test@email.com")
        self.assertEqual(counter_before_inc + 1, Subscriber.objects.count())

    def test_unique_email(self):
        Subscriber.objects.create(email="test@email.com")
        with self.assertRaises(IntegrityError):
            Subscriber.objects.create(email="test@email.com")
