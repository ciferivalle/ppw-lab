from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
def login(request):
    return render(request, "login.html", {})

def logout(request):
    response  = HttpResponseRedirect('/')
    response.delete_cookie('sessionid')
    return response
