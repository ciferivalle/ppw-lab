from django import forms
from .models import Profile
from praktikum import settings

class ProfileForm(forms.Form):
    fullname = forms.CharField(label="Full Name")
    username = forms.CharField(label="Username")
    email = forms.EmailField(label="Email")
    password = forms.CharField(label="Password")

    # class Meta:
    #     model = Profile
    #     fields = ['username', 'fullname', 'birth_date', 'password']

