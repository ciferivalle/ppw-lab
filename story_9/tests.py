from django.test import TestCase
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.urls import resolve
from .views import index
from django.test import Client
from .models import Favorited
from django.shortcuts import reverse
import requests

client = Client()

# Create your tests here.
class StatusViewTest(TestCase):
    """
    Test views' status code response
    """
    def test_status_view_get_return_302(self):
        """
        Test request get return 302
        """
        response = client.get('/story-9/')
        self.assertEqual(response.status_code, 302)

    def test_status_views_using_landing_func(self):
        """
        Test if a reverse using correct views function
        """
        found = resolve('/story-9/')
        self.assertEqual(found.func, index)

    def test_add_and_remove_to_and_from_favorite(self):
        response = client.post(reverse('story_9:add-to-favorite'), data={"liked" : "AJNsksjadudh"})
        print(response)
        self.assertEqual(response.status_code, 200)
        response = client.post(reverse('story_9:remove-from-favorite'), data={"liked" : "AJNsksjadudh"})
        self.assertEqual(response.status_code, 200)

class APIFirerTest(TestCase):
    def test_firing_get_json(self):
        response = client.get('/story-9/get-book-api/')
        self.assertNotEqual(response.status_code, 200)

class ModelTest(TestCase):
    def test_model_instance_created(self):
        counter_before_inc = Favorited.objects.count()
        liked_book = Favorited.objects.create(book_id="Juhsadu")
        self.assertEqual(counter_before_inc + 1, Favorited.objects.count())
