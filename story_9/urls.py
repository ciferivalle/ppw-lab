from django.contrib import admin
from django.urls import path, include
from .views import (index,
                    add_to_favorite,
                    remove_from_favorite,
                    index_partial,
                    get_book_json,
                    get_fav_counter)

urlpatterns = [
    path('', index, name='index'),
    path('get-book-json/', get_book_json, name='get-book-json'),
    path('get-fav-counter/', get_fav_counter, name='get-fav-counter'),
    path('partial/', index_partial, name='index-partial'),
    path('add-to-favorite/', add_to_favorite, name='add-to-favorite'),
    path('remove-from-favorite/', remove_from_favorite, name='remove-from-favorite'),
]
