import requests
import json
from django.shortcuts import render, redirect
from django.http import JsonResponse
# from .api_firer import *
from .models import Favorited
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
#BOOK_API_URL = "https://www.googleapis.com/books/v1/volumes?q=quitling"
#BOOK_LIST =  requests.get(BOOK_API_URL).json()
#BOOK_ID_LIST = [ BOOK_LIST['items'][book]['id'] for book in range(len(BOOK_LIST['items'])) ]

try:
    liked_books = request.session['liked_books']
except:
    liked_books = []


@csrf_exempt
def index(request):
    if(not request.user.is_authenticated):
        return redirect("/")
    response = {}

    if(request.user.is_authenticated):
        if(request.session['firstLogged']):
            liked_books = []
            request.session['liked_books'] = liked_books
            request.session['firstLogged'] = False

    for key, val in request.session.items():
        print("""   ============
        [{}]====[{}]
        ============""".format(key, val))

    # favorited = Favorited.objects.all()
    # favorited_id = [ fav.book_id for fav in favorited ]

    # response = {
    #     "favorited" : favorited_id,
    # }

    return render(request, 'index9.html', response)

def get_book_json(request):
    try:
        q = request.GET['q']
    except:
        q = ""

    BOOK_API_URL = "https://www.googleapis.com/books/v1/volumes?q=" + q
    BOOK_LIST =  requests.get(BOOK_API_URL).json()
    # BOOK_ID_LIST = [ BOOK_LIST['items'][book]['id'] for book in range(len(BOOK_LIST['items'])) ]
    BOOK_LIST['liked_books'] = liked_books
    return JsonResponse(BOOK_LIST)

def get_fav_counter(request):
    favorited = Favorited.objects.all()
    favorited_id = [ fav.book_id for fav in favorited ]
    response = {
        "fav-ids": favorited_id,
        "fav-counts": len(favorited_id),
        "status_code": 200,
    }
    return JsonResponse(response)


# below this used when implementing ajax request and save to database
@csrf_exempt
def index_partial(request):
    response = {}
    favorited = Favorited.objects.all()

    favorited_id = [ fav.book_id for fav in favorited ]

    # from django.conf import settings

    response = {
        "books" : BOOK_LIST,
        "favorited" : favorited_id,
    }

    return render(request, 'index9_partial.html', response)

@csrf_exempt
def add_to_favorite(request):
    if(request.method == "POST"):
        liked_book_id = request.POST['liked']
        if(liked_book_id not in liked_books):
            liked_books.append(liked_book_id)
            request.session['liked_books'] = liked_books

        for key, val in request.session.items():
            print("""   ============
            [{}]====[{}]
            ============""".format(key, val))

        return JsonResponse({"result" : "success", 'status' : 200}, safe=False)
        # return JsonResponse({"result" : "success", 'status' : 403}, safe=False)
    return JsonResponse({"result" : "No Get Method", 'status' : 403}, safe=False)

@csrf_exempt
def remove_from_favorite(request):
    if(request.method == "POST"):
        try:
            liked_book_id = request.POST['liked']
        except:
            return JsonResponse({"result" : "error occured", 'status' : 400}, safe=False)
        if(liked_book_id in liked_books):
            liked_books.remove(liked_book_id)
            request.session['liked_books'] = liked_books
        for key, val in request.session.items():
            print("""   ============
            [{}]====[{}]
            ============""".format(key, val))
        return JsonResponse({"result" : "success", 'status' : 200}, safe=False)
    return JsonResponse({"result" : "No Get Method", 'status' : 403}, safe=False)
